## 一 综述



## 二 原理

<div align=center>
    <img height ='200' src ="./res/enc2dec.svg"/>
    <center>图 encoder2decoder </center>
</div>

<div align=center>
    <img height ='300' src ="./res/attention-enc2dec-a.svg"/>
    <center>图 encoder2decoder </center>
</div>

## 三 实战
### 3.1 PYTORCH版本

### 3.2 原生版本


## 四 参考
[1] [RECURRENT NEURAL NETWORK REGULARIZATION](https://arxiv.org/pdf/1409.2329.pdf)
[2] [LONG SHORT TERM MEMORY WITH DYNAMIC SKIP CONNECTIONS ](https://arxiv.org/abs/1811.03873)
[2] [Understanding LSTM Networks](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)
[3] [Illustrated Guide to Recurrent Neural Networks](https://towardsdatascience.com/illustrated-guide-to-recurrent-neural-networks-79e5eb8049c9)
[3] [Illustrated Guide to LSTM’s and GRU’s: A step by step explanation](https://towardsdatascience.com/illustrated-guide-to-lstms-and-gru-s-a-step-by-step-explanation-44e9eb85bf21)
[4] [Recurrent Neural Network Tutorial, Part 4 – Implementing a GRU/LSTM RNN with Python and Theano](http://www.wildml.com/2015/10/recurrent-neural-network-tutorial-part-4-implementing-a-grulstm-rnn-with-python-and-theano/)
[5] [超生动图解LSTM和GRU](https://zhuanlan.zhihu.com/p/46981722)
[6] [完全图解RNN、RNN变体、Seq2Seq、Attention机制](https://zhuanlan.zhihu.com/p/28054589)
[6] [公式整理](https://blog.csdn.net/MoreAction_/article/details/107946186)
[6] [人人都能看懂的LSTM](https://zhuanlan.zhihu.com/p/32085405)
[] [史上最详细循环神经网络讲解（RNN/LSTM/GRU）](https://zhuanlan.zhihu.com/p/123211148)
[] [从反向传播推导到梯度消失and爆炸的原因及解决方案（从DNN到RNN，内附详细反向传播公式推导）](https://zhuanlan.zhihu.com/p/76772734)