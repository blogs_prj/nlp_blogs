## 一 综述

<div align=center>
    <img height ='150' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/机器对话.gif"/>
    <center>图 综述 </center>
</div>

## 二 原理

我们使用一个案例，输入“what time is it”。
<div align=center>
    <img height ='150' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/RNN-对话示例1.gif"/>
    <!-- <center>图 对话示例</center> -->
</div>
第一步将第一个单词what输入到RNN(这里在实际工程需要编码，onehot or word2vec)，并且产出当前的输出O1。并且输出当前隐藏状态。
<div align=center>
    <img height ='150' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/RNN-对话示例2.gif"/>
    <!-- <center>图 对话示例</center> -->
</div>
第二步，输入下个单词word，并且根据前一个输入得出的隐藏状态以及输入的单词得出当前结果o2。并且输出当前隐藏状态。
<div align=center>
    <img height ='150' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/RNN-对话示例3.gif"/>
    <!-- <center>图 对话示例</center> -->
</div>
重复以上过程，直到最后一个单词。我们可以通过RNN编码当前文本的信息。
<div align=center>
    <img height ='150' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/RNN-对话示例4.gif"/>
    <!-- <center>图 对话示例</center> -->
</div>
伪代码案例如下
<div align=center>
    <img height ='150' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/RNN-示例伪代码.png"/>
    <!-- <center>图 对话示例</center> -->
</div>

训练一段神经网络，一般有三个步骤，第一步是做好前项传播并且产出预测。第二步是得出预测与实际结果的误差，这里会根据不同应用场景选择不同的损失函数。第三步通过误差值反向传播更新整个网络节点参数。


### RNN

__图例__
<div align=center>
    <img height ='150' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/RNN-示例.png"/>
    <center>图 RNN</center>
</div>

__公式__

$   
    \left\{
    \begin{array}{l}
        h_t=tanh({W}_{hh}{h}_{t-1} + {W}_{xh}{x}_{t})\\
        y_t=({W}_{hy}*{h}_{t} )
    \end{array}
    \right.
$

优点：
* RNN适合处理序列数据，RNN考虑序列信息。
* RNN与CNN一起使用会有更好任务效果。

缺点：
* 梯度消失，梯度爆炸。
* RNN较其他CNN和全连接要用更多的显存空间，更难训练。
* 如果采用tanh、relu为激活函数，没法处理太长的序列。

### LSTM

__图例__

<div align=center>
    <img height ='200' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/LSTM-示例2.png"/>
    <center>图 LSTM </center>
</div>

__公式__

$   
    \left\{
    \begin{array}{l}
        i_t=\sigma(W^ix^t+U^ih_{t-1}) \\
        f_t=\sigma(W^fx^t+U^fh_{t-1}) \\
        o_t=\sigma(W^ox^t+U^oh_{t-1}) \\
        \tilde{c_t}=tanh(W^cx^t+U^ch_{t-1}) \\
        c_t=f^tc^{t-1}+i^t\tilde{c^t} \\
        h^t=o^t*tanh(c^t)\\
    \end{array}
    \right.
$

优点：
* A
* A
缺点：
* A
* A
* A

### GRU

__图例__

<div align=center>
    <img height ='200' src ="https://gitlab.com/blogs_prj/nlp_blogs/raw/master/rnn/res/GRU-示例1.png"/>
    <center>图 GRU </center>
</div>

__公式__

$   
    \left\{
    \begin{array}{l}
        z_t=\sigma(W_zx_t+U_zh_{t-1})=\sigma(W_z[h_{t-1},x_t]) \\
        r_t=\sigma(W_rx_t+U_rh_{t-1})=\sigma(W_r[h_{t-1},x_t]) \\
        \tilde{h_t}=tanh(Wx_t+Uh_{t-1})=tanh(W[r_th_{t-1},x_t])  \\
        c_t=f_tc_{t-1}+i_t\tilde{c_t} \\
        h_t=(1-z_t)\tilde{h_t}+ z_th_{t-1}\\
    \end{array}
    \right.
$

优点：
* A
* A
缺点：
* A
* A
* A

## 三 实战
### 3.1 PYTORCH版本

### 3.2 原生版本


## 四 参考
[1] [RECURRENT NEURAL NETWORK REGULARIZATION](https://arxiv.org/pdf/1409.2329.pdf)
[2] [LONG SHORT TERM MEMORY WITH DYNAMIC SKIP CONNECTIONS ](https://arxiv.org/abs/1811.03873)
[2] [Understanding LSTM Networks](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)
[3] [Illustrated Guide to Recurrent Neural Networks](https://towardsdatascience.com/illustrated-guide-to-recurrent-neural-networks-79e5eb8049c9)
[3] [Illustrated Guide to LSTM’s and GRU’s: A step by step explanation](https://towardsdatascience.com/illustrated-guide-to-lstms-and-gru-s-a-step-by-step-explanation-44e9eb85bf21)
[4] [Recurrent Neural Network Tutorial, Part 4 – Implementing a GRU/LSTM RNN with Python and Theano](http://www.wildml.com/2015/10/recurrent-neural-network-tutorial-part-4-implementing-a-grulstm-rnn-with-python-and-theano/)
[5] [超生动图解LSTM和GRU](https://zhuanlan.zhihu.com/p/46981722)
[6] [完全图解RNN、RNN变体、Seq2Seq、Attention机制](https://zhuanlan.zhihu.com/p/28054589)
[6] [公式整理](https://blog.csdn.net/MoreAction_/article/details/107946186)
[6] [人人都能看懂的LSTM](https://zhuanlan.zhihu.com/p/32085405)
[] [史上最详细循环神经网络讲解（RNN/LSTM/GRU）](https://zhuanlan.zhihu.com/p/123211148)
[] [从反向传播推导到梯度消失and爆炸的原因及解决方案（从DNN到RNN，内附详细反向传播公式推导）](https://zhuanlan.zhihu.com/p/76772734)